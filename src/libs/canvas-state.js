// This file basically handles draw history
// One per layer

class CanvasState {
	constructor () {
		this.actions = [];
		this.undoneActions = [];
	}

	setCanvas (canvas, color) {
		this.canvas = canvas;
		this.ctx = canvas.getContext("2d");

		this.ctx.fillStyle = color;
		this.ctx.fillRect(0, 0, 1000, 1000);
	}

	restore () {
		this.checkError();

		const img = new Image();
		img.src = this.actions[this.actions.length - 1];

		img.onload = () => {
			this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
			this.ctx.drawImage(img, 0, 0);
		};
	}

	save () {
		// Call this on mouse up
		this.checkError();

		this.actions.push(this.canvas.toDataURL());
	}

	undo () {
		this.checkError();

		const lastAction = this.actions.pop();

		if (lastAction) {
			this.undoneActions.push(lastAction);
			this.restore();
		}
	}

	redo () {
		// This reverses the last undo
		this.checkError();

		const lastUndo = this.undoneActions.pop();

		if (lastUndo) {
			this.actions.push(lastUndo);
			this.restore();
		}
	}

	checkError () {
		if (!this.canvas) {
			throw new Error("Canvas has not been set.");
		}
	}
};

export default CanvasState;
