import Shape from "../shape.js";

const square = new Shape({
	name: "Square"
});

export default square;
