export default [
	{
		name: "Color",
		value: "#ffffff",
		type: "color"
	},
	{
		name: "Size",
		value: 40,
		type: "number",
		options: {
			min: 1,
			max: 100
		}
	},
	{
		name: "Shape",
		value: "square",
		type: "selection",
		options: [
			"square",
			"circle"
		]
	}
];

// Angle option? Make it circular with normal up and down being center of circle
