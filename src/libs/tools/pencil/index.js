import Tool from "../tool";
import icon from "../grab/icon.png";
import options from "./options";

const pencil = new Tool({
	name: "Pencil",
	category: "Drawing",
	icon,
	cursor: icon,
	options
});

pencil.draw = () => {
	const ctx = pencil.getCanvasState().ctx;
	const canvas = pencil.getCanvasState().canvas;
	const size = pencil.getOption("Size");
	const color = pencil.getOption("Color");
	const shape = pencil.getOption("Shape");

	ctx.strokeStyle = "red";
	ctx.lineWidth = size;
	ctx.beginPath();
	ctx.moveTo(pencil.drawPoints[0].x - canvas.offsetLeft, pencil.drawPoints[0].y - canvas.offsetTop);

	pencil.drawPoints.forEach(point => {
		ctx.lineTo(point.x - canvas.offsetLeft, point.y - canvas.offsetTop);
	});

	ctx.stroke();
};

pencil.drawCursor = (e) => {
	const canvas = e.target;
	const ctx = canvas.getContext("2d");
	const size = pencil.getOption("Size");
	const color = pencil.getOption("Color");
	const shape = pencil.getOption("Shape");

	// Just clear the cursor
	if (e.type === "mouseout") {
		ctx.clearRect(0, 0, canvas.width, canvas.height);
		return;
	}

	// Clear canvas ready to draw cursor
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	// Draw cursor
	ctx.beginPath();
	ctx.fillStyle = color;
	ctx.fillRect(e.layerX - size / 2, e.layerY - size / 2, size, size);
	ctx.closePath();
};

pencil.setEvent("mousemove", (e) => {
	pencil.drawCursor(e);

	if (pencil.isDrawing) {
		pencil.drawPoints.push({
			x: e.layerX,
			y: e.layerY
		});

		pencil.draw(e);
	}
});

pencil.setEvent("mouseout", (e) => {
	pencil.drawCursor(e);

	if (pencil.isDrawing) {
		pencil.isDrawing = false;
		pencil.drawPoints = [];

		pencil.getCanvasState().save();
	}
});

pencil.setEvent("mousedown", (e) => {
	if (!pencil.drawPoints) {
		pencil.drawPoints = [];
	}

	pencil.drawPoints.push({
		x: e.layerX,
		y: e.layerY
	});

	pencil.draw(e);

	pencil.isDrawing = true;
});

pencil.setEvent("mouseup", (e) => {
	pencil.isDrawing = false;
	pencil.drawPoints = [];
	pencil.getCanvasState().save();
});

export default pencil;
