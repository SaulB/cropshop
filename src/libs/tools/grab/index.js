import icon from "./icon.png";
import options from "./options";

export default {
	name: "Grab",
	category: "Selection",
	priority: 3,
	icon,
	options
};
