import store from "@/store/index";

class Tool {
	constructor ({ name, category, icon, cursor, options }) {
		// Menu info
		this.name = name;
		this.category = category;
		this.icon = icon;
		this.cursor = cursor;
		this.options = options;

		// Events
		this.events = {
			mousemove () {},
			mouseout () {},
			mousedown () {},
			mouseup () {}
		};
	}

	setEvent (eventName, eventFunction) {
		this.events[eventName] = eventFunction;
	}

	getOption (optionName) {
		return this.options.find(
			option => option.name === optionName
		).value;
	}

	getCanvasState () {
		// TODO: don't cache this if it doesn't detect layer changes.
		// This is a pain to access as getters don't seem to work outside of vue.
		if (this.activeLayer) {
			// Just so we don't constantly use unessessary find loops
			return this.activeLayer.canvasState;
		}

		const fileModule = store.state.files;
		const activeFile = fileModule.files.find(file => file.active);
		const layers = activeFile.layerStore.state.layers;
		const activeLayer = layers.find(layer => layer.active);
		const canvasState = activeLayer.canvasState;

		this.activeLayer = activeLayer;

		return canvasState;
	}
};

export default Tool;
