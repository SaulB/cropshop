// This file holds the functions for registering tools.

import Vue from "vue";

// Require all tools
const tools = ((r) => {
	// Filters out all files but index files one sub directory deep
	const files = r.keys().filter(file => {
		return /^(\.\/[a-zA-Z0-9-]+\/index\.js)/.test(file);
	});

	return files.map(file => r(file));
})(require.context("@/libs/tools", true, /\.js$/));

export default {
	namespaced: true,

	state: {
		tools: [],
		color: "#ffffff" // TODO update the active tool to use this color if it uses color
	},

	getters: {
		getActiveTool (state) {
			return state.tools.find(tool => tool.active);
		},

		getCategories (state) {
			return state.tools.map(
				tool => tool.category
			).filter((tool, index, self) => (
				index === self.indexOf(tool)
			));
		},

		getToolsFromCategory: (state) => (category) => {
			return state.tools.filter(tool => tool.category === category);
		}
	},

	actions: {
		initialize ({ dispatch }) {
			tools.forEach(tool => dispatch("registerTool", tool.default));
		},

		registerTool ({ commit }, tool) {
			if (!tool) {
				console.error("Failed to register shape");
				return;
			}

			// TODO: verify the tool is a valid tool
			console.log(`Registering tool: ${tool.name}`);
			commit("addTool", tool);
		},

		setActiveTool ({ commit, state }, tool) {
			commit("deactivateActiveTool");
			commit("activateTool", tool);
		}
	},

	mutations: {
		addTool (state, tool) {
			state.tools.push(tool);
		},

		activateTool (state, tool) {
			if (tool) {
				Vue.set(tool, "active", true);
			}
		},

		deactivateActiveTool (state) {
			// Work around to get vue to detect the change...
			const index = state.tools.map(tool => tool.active).indexOf(true);

			if (index !== -1) {
				// Set the whole object with itself and one property differnt
				Vue.set(state.tools, index, {
					...state.tools[index],
					active: false
				});
			}
		}
	}
};
