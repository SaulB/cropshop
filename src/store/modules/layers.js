import CanvasState from "@/libs/canvas-state";

export default () => ({
	namespaced: true,

	state: {
		layers: [],
		file: null // This is a ref to the parent
	},

	getters: {
		getActiveLayer: (state) => state.layers.find(layer => layer.active)
	},

	actions: {
		initializeLayers ({ dispatch, commit }, file) {
			// Set our layer reference
			commit("setFile", file);

			// Set our first layer
			dispatch("createLayer", "Base Layer");
		},

		createLayer ({ state, commit }, name) {
			commit("addLayer", {
				name,
				canvasState: new CanvasState()
			});
		}
	},

	mutations: {
		addLayer (state, layer) {
			layer.active = true;
			state.layers.push(layer);
		},

		setFile (state, file) {
			state.file = file;
		}
	}
});
