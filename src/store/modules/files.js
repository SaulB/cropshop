import Vuex from "vuex";
import layers from "./layers";

export default {
	namespaced: true,

	state: {
		files: []
	},

	getters: {
		getActiveFile: (state) => state.files.find(file => file.active),

		getFileByName: (state) => (name) => state.files.find(file => file.active)
	},

	actions: {
		createFile ({ state, getters, commit, dispatch }, file) {
			// Set this file as the active one
			dispatch("setActiveFile", file);

			// Create the store
			file.layerStore = new Vuex.Store(layers());

			// Create the initial layer
			file.layerStore.dispatch("initializeLayers", file);

			// Finally add the file to the store
			commit("addFile", file);
		},

		setActiveFile ({ state, getters, commit }, file) {
			commit("setActiveFile", {
				activeFile: getters.getActiveFile,
				file
			});
		}
	},

	mutations: {
		addFile (state, file) {
			state.files.push(file);
		},

		setActiveFile (state, { activeFile, file }) {
			if (activeFile) {
				activeFile.active = false;
			}

			file.active = true;
		}
	}
};
