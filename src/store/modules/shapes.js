// This file holds the functions for registering shapes for use with tools.

import Vue from "vue";

// Require all shapes
const shapes = ((r) => {
	// Filters out all files but index files one sub directory deep
	const files = r.keys().filter(file => {
		return /^(\.\/[a-zA-Z0-9-]+\/index\.js)/.test(file);
	});

	return files.map(file => r(file));
})(require.context("@/libs/shapes", true, /\.js$/));

export default {
	namespaced: true,

	state: {
		shapes: []
	},

	getters: {
		getActiveShape (state) {
			return state.shapes.find(shape => shape.active);
		},

		getCategories (state) {
			return state.shapes.map(
				shape => shape.category
			).filter((shape, index, self) => (
				index === self.indexOf(shape)
			));
		},

		getShapesFromCategory: (state) => (category) => {
			return state.shapes.filter(shape => shape.category === category);
		}
	},

	actions: {
		initialize ({ dispatch }) {
			shapes.forEach(shape => dispatch("registerShape", shape.default));
		},

		registerShape ({ commit }, shape) {
			if (!shape) {
				console.error("Failed to register shape");
				return;
			}

			// TODO: verify the shape is a valid shape
			console.log(`Registering shape: ${shape.name}`);
			commit("addShape", shape);
		},

		setActiveShape ({ commit, state }, shape) {
			commit("deactivateActiveShape");
			commit("activateShape", shape);
		}
	},

	mutations: {
		addShape (state, Shape) {
			state.shapes.push(Shape);
		},

		activateShape (state, Shape) {
			if (Shape) {
				Vue.set(Shape, "active", true);
			}
		},

		deactivateActiveShape (state) {
			// Work around to get vue to detect the change...
			const index = state.shapes.map(shape => shape.active).indexOf(true);

			if (index !== -1) {
				// Set the whole object with itself and one property differnt
				Vue.set(state.shapes, index, {
					...state.shapes[index],
					active: false
				});
			}
		}
	}
};
