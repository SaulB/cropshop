import Vue from "vue";
import Vuex from "vuex";
import files from "./modules/files";
import tools from "./modules/tools";
import shapes from "./modules/shapes";

Vue.use(Vuex);

const store = new Vuex.Store({
	modules: {
		files,
		tools,
		shapes
	},

	state: {

	},

	getters: {

	},

	actions: {

	},

	mutations: {

	}
});

store.dispatch("tools/initialize");
store.dispatch("shapes/initialize");

export default store;
